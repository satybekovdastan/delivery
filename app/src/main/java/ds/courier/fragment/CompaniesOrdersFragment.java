package ds.courier.fragment;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;

import ds.courier.R;
import ds.courier.adapter.RVCompanyOrderAdapter;
import ds.courier.api.ApiService;
import ds.courier.api.RetroClient;
import ds.courier.model.Order;

/**
 * Created by Dastan on 28.02.2017.
 */

public class CompaniesOrdersFragment extends android.support.v4.app.Fragment {

    public CompaniesOrdersFragment() {
    }

    RVCompanyOrderAdapter rvCompanyOrderAdapter;
    ArrayList<Order> orderArrayList;
    RecyclerView recyclerView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_companies_orders, container, false);

        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        orderArrayList = new ArrayList<>();
        rvCompanyOrderAdapter = new RVCompanyOrderAdapter(getActivity(), orderArrayList, false);
        recyclerView.setAdapter(rvCompanyOrderAdapter);
        getJsonOrders();
        return view;
    }


    public void getJsonOrders() {

        ApiService api = RetroClient.getApiService(getActivity());
        retrofit2.Call<ArrayList<Order>> call;
        call = api.getOrders();
        call.enqueue(new retrofit2.Callback<ArrayList<Order>>() {
            @Override
            public void onResponse(retrofit2.Call<ArrayList<Order>> call, final retrofit2.Response<ArrayList<Order>> response) {
                if (getActivity()!= null) {
                    if (response.isSuccessful()) {
                        ArrayList<Order> list = response.body();
                        orderArrayList.addAll(response.body());
                        rvCompanyOrderAdapter.notifyDataSetChanged();
                        if (list.size() == 0) {
                            Toast.makeText(getActivity(), "Пусто!", Toast.LENGTH_SHORT).show();
//                        text_placeholder.setText("Мы пополняем базу данных категории «" + response.body().getTitle() + "»");
                        }
                    }
                }
            }

            @Override
            public void onFailure(retrofit2.Call<ArrayList<Order>> call, Throwable t) {
                if (getActivity() != null) {
                    Toast.makeText(getActivity(), t.getMessage() + "", Toast.LENGTH_SHORT).show();
                    Log.e("TAG", "FAIL");
                }
            }
        });
    }

}
