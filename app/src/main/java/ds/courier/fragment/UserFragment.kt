package ds.courier.fragment

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.squareup.picasso.Picasso

import java.io.IOException
import java.util.ArrayList

import ds.courier.R
import ds.courier.TokenPrefsHelper
import ds.courier.activity.AuthenticationActivity
import ds.courier.activity.RegistrationActivity
import ds.courier.api.ApiService
import ds.courier.api.RetroClient
import ds.courier.model.Account
import ds.courier.model.Car
import kotlinx.android.synthetic.main.fragment_user.*
import kotlinx.android.synthetic.main.fragment_user.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by Dastan on 28.02.2017.
 */

class UserFragment : android.support.v4.app.Fragment() {

    private val tokenPrefsHelper = TokenPrefsHelper()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {


        val view = inflater!!.inflate(R.layout.fragment_user, container, false)

        getAccount()

        view.btnLogout.setOnClickListener {
            tokenPrefsHelper.getDeleteToken(this!!.activity!!)
            val intent = Intent(activity, AuthenticationActivity::class.java)
            startActivity(intent)
            activity!!.finish()
        }

        return view
//        return inflater.inflate(R.layout.fragment_user, container, false)
    }


    fun getAccount() {


        val api = RetroClient.getApiService(activity)
        val call: Call<Account>
        call = api.account
        call.enqueue(object : Callback<Account> {
            override fun onResponse(call: Call<Account>, response: Response<Account>) {
                if (response.isSuccessful) {
                    first_name.setText(response.body().first_name)
                    sur_name.setText(response.body().last_name)
                    edit_phone.setText(response.body().phone)
                    edit_car_type.setText(response.body().car_type)

                    if (response.body().photo == null) {

                    }else{
                        Log.e("PHOTO", response.body().photo)
                        person_avatar.visibility =View.VISIBLE
                        imgProfile.visibility = View.INVISIBLE
                        Picasso.get().load("http://37.46.128.80:8000/"+response.body().photo).into(person_avatar)
                    }


                    if (response.body().document == null) {

                    }else{
                        Picasso.get().load("http://37.46.128.80:8000/"+response.body().document).into(license_photo)
                    }

                } else {
                    try {
                        Log.e("ERROR", response.errorBody().string())
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }

                }
            }

            override fun onFailure(call: Call<Account>, t: Throwable) {
                Log.e("FAIL", t.message)
            }
        })
    }


}
