package ds.courier.fragment;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import ds.courier.R;
import ds.courier.activity.OSMapsActivity;
import ds.courier.activity.WorkMapActivity;

public class FragmentSelect extends android.support.v4.app.Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment, container, false);

        Button onClick = view.findViewById(R.id.onClick);
        Button onClickOpen = view.findViewById(R.id.onClickOpen);

        onClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?daddr=42.871959,74.611437"));
                startActivity(intent);
//                startActivity(new Intent(getActivity(), MapsActivity.class));
            }
        });

        onClickOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), WorkMapActivity.class));
            }
        });

        return view;
    }
}
