package ds.courier.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ds.courier.R;
import ds.courier.activity.OSMapsActivity;
import ds.courier.api.ApiService;
import ds.courier.api.RetroClient;
import ds.courier.model.Address;
import ds.courier.model.Order;
import ds.courier.model.Product;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RVAddressAdapter extends RecyclerView.Adapter<RVAddressAdapter.PersonViewHolder> {

    Context context;
    Address vse;

    public class PersonViewHolder extends RecyclerView.ViewHolder {

        TextView tvProducts, tv_title, tv_distance;
        Button btn_delivered;
        Button btn_navigation;

        PersonViewHolder(View itemView) {
            super(itemView);
            tvProducts = itemView.findViewById(R.id.tv_products);
            tv_title = itemView.findViewById(R.id.tv_title);
            btn_delivered = itemView.findViewById(R.id.btn_delivered);
            btn_navigation = itemView.findViewById(R.id.btn_navigation);
            tv_distance = itemView.findViewById(R.id.tv_distance);
            btn_delivered.setVisibility(View.GONE);
            btn_navigation.setVisibility(View.GONE);
            itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
//                    Intent intent = new Intent(context, OSMapsActivity.class);
                    vse = listVse.get(getAdapterPosition());
//                    intent.putExtra("id", vse.getId());
//                    Activity activity = (Activity) context;
//                    activity.startActivityForResult(intent, 123);
//                    activity.overridePendingTransition(0, 0);
//                    ((OSMapsActivity) context).getMapsCenterMarker(vse.getGeolocation());
                }
            });

            btn_delivered.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case DialogInterface.BUTTON_POSITIVE:

                                    break;

                                case DialogInterface.BUTTON_NEGATIVE:
                                    //No button clicked
                                    vse = listVse.get(getAdapterPosition());
                                    postComplete(vse.getId(), getAdapterPosition());
                                    break;
                            }
                        }
                    };

                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setMessage("Вы завершили достваку к этому аддресу?").setPositiveButton("НЕТ", dialogClickListener)
                            .setNegativeButton("ДА", dialogClickListener).show();

                }
            });


        }

        public void postComplete(int id, int position) {
            final ProgressDialog uploading;
            uploading = ProgressDialog.show(context, "Loading", "Please wait...", false, false);

            final ApiService api = RetroClient.getApiService(context);
            Call<ResponseBody> call;
            call = api.postCompleteAdrres(id);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    uploading.dismiss();
                    Log.e("RES_CODE", response.code() + "");
                    if (response.isSuccessful()) {
                        try {
                            Log.e("RES_CODE", response.code() + " " + response.body().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        removeAt(position);
                    } else {
                        uploading.dismiss();
                        try {
                            Log.e("ERROR", response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    uploading.dismiss();
                    Log.e("FAIL", t.getMessage());
                }
            });
        }


    }

    public void removeAt(int position) {
        listVse.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, listVse.size());
    }

    List<Address> listVse;

    public RVAddressAdapter(Context context, List<Address> listVse) {
        this.listVse = listVse;
        this.context = context;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_address, viewGroup, false);
        PersonViewHolder pvh = new PersonViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(final PersonViewHolder personViewHolder, int i) {
        vse = new Address();
        vse = listVse.get(i);

        String prod = "";
        int pos = 0;
        for (Product product : vse.getProducts()) {
            pos += 1;
            prod += pos + ". " + product.getTitle() + " - " + product.getWeight() + "\n";
        }

        double lat = 0, lon = 0;
        List<String> elephantList = Arrays.asList(vse.getGeolocation().split(","));
        for (int j = 0; j < elephantList.size(); j++) {
            lat = Double.parseDouble(elephantList.get(0));
            lon = Double.parseDouble(elephantList.get(1));
        }

        Location clientLocation = new Location("");
        clientLocation.setLatitude(lat);
        clientLocation.setLongitude(lon);

        Location myLocation = new Location("");
        myLocation.setLatitude(42.871959);
        myLocation.setLongitude(74.611437);

        clientLocation.distanceTo(myLocation);

        personViewHolder.tv_title.setText(vse.getAddress() + "");
        personViewHolder.tvProducts.setText(prod);
        personViewHolder.tv_distance.setText(clientLocation.distanceTo(myLocation)+" км");
    }

    @Override
    public int getItemCount() {
        return listVse.size();
    }

}