package ds.courier.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import org.json.JSONObject;

import java.util.List;

import ds.courier.R;
import ds.courier.activity.OSMapsActivity;
import ds.courier.activity.OrdersOSMapsActivity;
import ds.courier.api.ApiService;
import ds.courier.api.RetroClient;
import ds.courier.model.Order;


public class RVCompanyOrderAdapter extends RecyclerView.Adapter<RVCompanyOrderAdapter.PersonViewHolder> {

    Context context;
    Order vse;

    public class PersonViewHolder extends RecyclerView.ViewHolder {

        TextView text_address, tv_data_create, tv_title;

        PersonViewHolder(View itemView) {
            super(itemView);
            text_address = itemView.findViewById(R.id.tv_address);
            tv_data_create = itemView.findViewById(R.id.tv_data_create);
            tv_title = itemView.findViewById(R.id.tv_title);

            itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Intent intent;
                    if (myOrder) {
                        intent = new Intent(context, OSMapsActivity.class);
                    }else {
                        intent = new Intent(context, OrdersOSMapsActivity.class);
                    }
                    vse = listVse.get(getAdapterPosition());
                    intent.putExtra("id", vse.getId());
                    intent.putExtra("myOrder", myOrder);
                    Activity activity = (Activity) context;
                    activity.startActivityForResult(intent, 123);
                    activity.overridePendingTransition(0, 0);
                }
            });

        }

    }

    List<Order> listVse;
    boolean myOrder;

    public RVCompanyOrderAdapter(Context context, List<Order> listVse, boolean myOrder) {
        this.listVse = listVse;
        this.context = context;
        this.myOrder = myOrder;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_copany_order, viewGroup, false);
        PersonViewHolder pvh = new PersonViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(final PersonViewHolder personViewHolder, int i) {
        vse = new Order();
        vse = listVse.get(i);
        personViewHolder.text_address.setText(vse.getAddresses()+"");
        personViewHolder.tv_title.setText(vse.getTitle()+"");
        personViewHolder.tv_data_create.setText(vse.getCreatedAt()+"");

    }

    @Override
    public int getItemCount() {
        return listVse.size();
    }

}