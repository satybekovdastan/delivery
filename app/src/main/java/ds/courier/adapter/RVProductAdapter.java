package ds.courier.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ds.courier.R;
import ds.courier.model.Product;


public class RVProductAdapter extends RecyclerView.Adapter<RVProductAdapter.PersonViewHolder> {

    Context context;
    Product vse;

    public class PersonViewHolder extends RecyclerView.ViewHolder {

        TextView tv_weight, tv_title;

        PersonViewHolder(View itemView) {
            super(itemView);
            tv_weight = itemView.findViewById(R.id.tv_weight);
            tv_title = itemView.findViewById(R.id.tv_title);

            itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
//                    Intent intent = new Intent(context, OSMapsActivity.class);
//                    vse = listVse.get(getAdapterPosition());
//                    intent.putExtra("id", vse.getId());
//                    Activity activity = (Activity) context;
//                    activity.startActivityForResult(intent, 123);
//                    activity.overridePendingTransition(0, 0);
                }
            });

        }

    }

    List<Product> listVse;

    public RVProductAdapter(Context context, List<Product> listVse) {
        this.listVse = listVse;
        this.context = context;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_product, viewGroup, false);
        PersonViewHolder pvh = new PersonViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(final PersonViewHolder personViewHolder, int i) {
        vse = new Product();
        vse = listVse.get(i);
        personViewHolder.tv_weight.setText(vse.getWeight()+"");
        personViewHolder.tv_title.setText(vse.getTitle()+"");

    }

    @Override
    public int getItemCount() {
        return listVse.size();
    }

}