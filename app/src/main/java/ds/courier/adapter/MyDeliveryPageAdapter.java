package ds.courier.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import ds.courier.fragment.CompaniesOrdersFragment;
import ds.courier.fragment.FragmentSelect;
import ds.courier.fragment.HistoryOrdersFragment;
import ds.courier.fragment.MyOrdersFragment;

public class MyDeliveryPageAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    String type;

    public MyDeliveryPageAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                MyOrdersFragment tab1 = new MyOrdersFragment();
                return tab1;
            case 1:
                HistoryOrdersFragment tab2 = new HistoryOrdersFragment();
                return tab2;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}