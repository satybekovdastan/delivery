package ds.courier.model;

public class Car {
    int id;
    String title;
    int min_size;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getMin_size() {
        return min_size;
    }

    public void setMin_size(int min_size) {
        this.min_size = min_size;
    }
}
