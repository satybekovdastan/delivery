package ds.courier;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import ds.courier.fragment.CompaniesOrdersFragment;
import ds.courier.fragment.FragmentSelect;

public class PageAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    String type;

    public PageAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                CompaniesOrdersFragment tab1 = new CompaniesOrdersFragment();
                return tab1;
            case 1:
                FragmentSelect tab2 = new FragmentSelect();
                return tab2;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}