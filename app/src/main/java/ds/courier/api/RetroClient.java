package ds.courier.api;

import android.content.Context;
import android.util.Log;
import java.io.IOException;

import ds.courier.TokenPrefsHelper;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetroClient {

    static Context context;

    static private TokenPrefsHelper tokenPrefsHelper = new TokenPrefsHelper();

    private static final String ROOT_URL = "http://37.46.128.80:8000/";

    private static OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
        @Override
        public Response intercept(Chain chain) throws IOException {
            String key = null;
            try {
                key = tokenPrefsHelper.getToken(context);
                if (key == null || key.equals("")) {
                    key = "";
                } else {
                    key = "Token " + key;
                    Log.e("TOKEN ", key);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            
            Request request = chain.request().newBuilder()
                    .addHeader("Authorization", key)
                    .build();
            return chain.proceed(request);
        }
    }).build();


    private static Retrofit getRetrofitInstance() {
        return new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
    }

    public static ApiService getApiService(Context context2) {
        context = context2;
        return getRetrofitInstance().create(ApiService.class);
    }


}