package ds.courier.api;


import java.util.ArrayList;

import ds.courier.model.Account;
import ds.courier.model.Car;
import ds.courier.model.Key;
import ds.courier.model.Order;
import ds.courier.model.OrderDetail;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface ApiService {

    @FormUrlEncoded
    @POST("/api/couriers/")
    Call<Key> postCreateCourier(@Field("first_name") String first_name,
                                @Field("last_name") String last_name,
                                @Field("phone") String phone,
                                @Field("car_type") String car_type,
                                @Field("password") String password);


    @POST("/api/accounts/signup/")
    @Multipart
    Call<Key> postCreateCourierMulipart(@Part("first_name") RequestBody first_name,
                                        @Part("last_name") RequestBody last_name,
                                        @Part("phone") RequestBody phone,
                                        @Part("car_type") RequestBody car_type,
                                        @Part("password") RequestBody password,
                                        @Part("type") RequestBody courier,
                                        @Part() MultipartBody.Part photo,
                                        @Part() MultipartBody.Part document);


    @FormUrlEncoded
    @POST("/api/accounts/login/")
    Call<Key> postLogin(@Field("username") String username,
                        @Field("password") String password);

    @GET("/api/orders/accept/{id}")
    Call<Order> postTake(@Path("id") int id);

    @GET("/api/reject/{id}")
    Call<Order> postRejectOrder(@Path("id") int id);


    @FormUrlEncoded
    @POST("/api/reject/")
    Call<Order> postReject(@Field("comment") String comment,
                        @Field("order") int  orderId);

    @GET("api/customers/")
    Call<ResponseBody> getCustomers();

    @GET("api/cars/")
    Call<ArrayList<Car>> getCars();

    @GET("/api/accounts/profile/")
    Call<Account> getAccount();

    @GET("api/orders/")
    Call<ArrayList<Order>> getOrders();

    @GET("api/accounts/orders/")
    Call<ArrayList<Order>> getMyOrders();

    @GET("/api/accounts/history/")
    Call<ArrayList<Order>> getHistory();

    @GET("api/orders/{id}")
    Call<OrderDetail> getOrderDetail(@Path("id") int id);

//    @GET("expenses")
//    Call<ArrayList<CategoryExpenses>> getExpenses();

    @POST("api/history/{id}/add/")
    @Multipart
    Call<ResponseBody> addHistory(@Path("id") int id,
                                  @Part("comment") RequestBody comment,
                                  @Part() MultipartBody.Part audio);

    @GET("/api/addresses/complete/{id}/")
    Call<ResponseBody> postCompleteAdrres(@Path("id") int id);

}
