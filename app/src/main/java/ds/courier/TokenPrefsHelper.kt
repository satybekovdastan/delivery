package ds.courier

import android.content.Context
import android.content.SharedPreferences

class TokenPrefsHelper {

    var PREFS_NAME = "Prefs"
    var PREFS_KEY = "Token"

    fun saveToken(context: Context, token: String) {
        val settings: SharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
        val editor: SharedPreferences.Editor
        editor = settings.edit()
        editor.putString(PREFS_KEY, token)
        editor.commit()
    }

    fun getToken(context: Context): String? {
        var settings: SharedPreferences? = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
        var value: String? = settings!!.getString(PREFS_KEY, null)
        return value
    }


    fun getDeleteToken(context: Context) {
        val settings: SharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
        val editor: SharedPreferences.Editor
        editor = settings.edit()
        editor.remove(PREFS_KEY)
        editor.commit()
    }

}
