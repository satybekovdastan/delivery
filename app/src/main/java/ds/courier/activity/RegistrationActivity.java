package ds.courier.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Base64OutputStream;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import ds.courier.RealPathUtil;
import ds.courier.TokenPrefsHelper;
import ds.courier.model.Car;
import ds.courier.model.Key;
import ds.courier.R;
import ds.courier.api.ApiService;
import ds.courier.api.RetroClient;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrationActivity extends AppCompatActivity {


    public static final int REQUEST_IMAGE = 100;
    public static final int REQUEST_PERMISSION = 200;
    private String imageFilePath = "";

    ImageView imgProfile, license_photo, person_avatar;
    EditText first_name, sur_name;
    String firstName, surName, token;
    boolean img = false;
    boolean imgLicense = false;
    int camera = 0;
    File file2;
    Spinner spinner;
    String phone;
    int idCar = 0;
    File image;

    ArrayList<Car> carArrayList = new ArrayList<>();
    List<String> carArrayListName = new ArrayList<>();
    MultipartBody.Part filePhoto;
    MultipartBody.Part fileDocument;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        checkAndRequestPermissions();
        imgProfile = (ImageView) findViewById(R.id.imgProfile);
        license_photo = (ImageView) findViewById(R.id.license_photo);
        person_avatar = (ImageView) findViewById(R.id.person_avatar);
        Button button = (Button) findViewById(R.id.btnNext);
        first_name = (EditText) findViewById(R.id.first_name);
        sur_name = (EditText) findViewById(R.id.sur_name);

        phone = getIntent().getStringExtra("phone");


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean bool = true;
                firstName = first_name.getText().toString();
                surName = sur_name.getText().toString();

                if (!isValidName(firstName)) {
                    first_name.setError("Заполните поле");
                    bool = false;
                }
//                if (!isValidName(surName)) {
//                    sur_name.setError("Заполните поле");
//                    bool = false;
//                }
                img = true;
                imgLicense = true;
                if (img == false) {
                    bool = false;
                    Toast.makeText(RegistrationActivity.this, "Загрузите аватар!", Toast.LENGTH_SHORT).show();
                }
                if (imgLicense == false) {
                    bool = false;
                    Toast.makeText(RegistrationActivity.this, "Загрузите фото ВП!", Toast.LENGTH_SHORT).show();
                }

                if (idCar == 0) {
                    bool = false;
                    Toast.makeText(RegistrationActivity.this, "Выберите тип Машин!", Toast.LENGTH_SHORT).show();
                }
                Log.e("BOOL", bool + "");
                if (bool == true) {
//                    SendJsonDataToServerProfile();
                    postCreate();
                }
            }
        });

        imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                camera = 1;
                openCameraIntent();

            }
        });

        person_avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                camera = 1;
                openCameraIntent();
            }
        });


        license_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                camera = 2;
                openCameraIntent();
            }
        });

        if (CheckPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

        } else {
            RequestPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE, 123);
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_PERMISSION);
        }

        spinner = findViewById(R.id.spinner);

        getSettings();

    }

    public void getSettings() {
        final ApiService api = RetroClient.getApiService(this);
        Call<ArrayList<Car>> call;
        call = api.getCars();
        call.enqueue(new Callback<ArrayList<Car>>() {
            @Override
            public void onResponse(Call<ArrayList<Car>> call, Response<ArrayList<Car>> response) {
                if (response.isSuccessful()) {
                    carArrayList = new ArrayList<>();
                    Car car1 = new Car();
                    car1.setId(0);
                    car1.setTitle("Тип машины:");
                    carArrayList.add(car1);
                    carArrayList.addAll(response.body());
                    for (Car car : carArrayList) {
                        carArrayListName.add(car.getTitle());
                    }

                    final ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(RegistrationActivity.this, R.layout.spiener_item, carArrayListName);
                    dataAdapter.setDropDownViewResource(R.layout.spiener_dropdown);
                    spinner.setAdapter(dataAdapter);

                    spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view,
                                                   int position, long id) {
                            String category = spinner.getSelectedItem().toString();
                            idCar = carArrayList.get(position).getId();

                            Log.e("SPINER", category + " " + idCar);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> arg0) {
                        }
                    });

                } else {
                    try {
                        Log.e("ERROR", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Car>> call, Throwable t) {
                Log.e("FAIL", t.getMessage());
            }
        });
    }


    public void postCreate() {


        final ProgressDialog uploading;
        uploading = ProgressDialog.show(RegistrationActivity.this, "Loading", "Please wait...", false, false);


        final ApiService api = RetroClient.getApiService(this);
        Call<Key> call;
//        phone = "+996706530303";
        Log.e("POST", "" + firstName + " " + surName + " " + idCar + " " + phone + " " + phone+" f "+filePhoto+" d "+fileDocument);

//        MultipartBody.Part filePhoto = MultipartBody.Part.createFormData("photo", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));
//        MultipartBody.Part fileDocument = MultipartBody.Part.createFormData("photo", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));
        RequestBody firstName2 = RequestBody.create(MediaType.parse("text/plain"), firstName);
        RequestBody surName2 = RequestBody.create(MediaType.parse("text/plain"), surName);
        RequestBody phone2 = RequestBody.create(MediaType.parse("text/plain"), phone);
        RequestBody couerier = RequestBody.create(MediaType.parse("text/plain"), "courier");
        RequestBody idCar2 = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(idCar));

        call = api.postCreateCourierMulipart(firstName2, surName2, phone2, idCar2, phone2, couerier, filePhoto, fileDocument);
        call.enqueue(new Callback<Key>() {
            @Override
            public void onResponse(Call<Key> call, Response<Key> response) {
                Log.e("CODE", response.code() + "");
                uploading.dismiss();

                if (response.isSuccessful()) {
                    TokenPrefsHelper tokenPrefsHelper = new TokenPrefsHelper();
                    tokenPrefsHelper.saveToken(RegistrationActivity.this, response.body().getToken());
                    startActivity(new Intent(RegistrationActivity.this, MainActivity.class));
                    finish();
                } else {

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Log.e("jObjError", jObjError + "");
                        Toast.makeText(RegistrationActivity.this, jObjError.getString("phone"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(RegistrationActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Key> call, Throwable t) {
                uploading.dismiss();
                Log.e("FAIL", t.getMessage());
            }
        });
    }


    private boolean isValidName(String name) {
        if (name != null && name.length() > 1) {
            return true;
        }
        return false;
    }


    private void openCameraIntent() {
        Intent pictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (pictureIntent.resolveActivity(getPackageManager()) != null) {

            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
            file2 = photoFile;
            Uri photoUri = FileProvider.getUriForFile(this, getPackageName() + ".provider", photoFile);
            pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
            startActivityForResult(pictureIntent, REQUEST_IMAGE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_PERMISSION && grantResults.length > 0) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Thanks for granting Permission", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == RESULT_OK) {
                if (camera == 1) {
                    imgProfile.setVisibility(View.GONE);
                    person_avatar.setVisibility(View.VISIBLE);
                    person_avatar.setImageURI(Uri.parse(imageFilePath));
                    filePhoto = MultipartBody.Part.createFormData("photo", file2.getName(), RequestBody.create(MediaType.parse("image/*"), file2));

                } else {
                    license_photo.setImageURI(Uri.parse(imageFilePath));
                    fileDocument = MultipartBody.Part.createFormData("document", file2.getName(), RequestBody.create(MediaType.parse("image/*"), file2));

                }
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "You cancelled the operation", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private File createImageFile() throws IOException {

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        image = File.createTempFile(imageFileName, ".jpg", storageDir);

        imageFilePath = image.getAbsolutePath();
        return image;
    }


    @SuppressLint("NewApi")
    public static String getRealPathFromURI_API19(Context context, Uri uri) {
        String filePath = "";
        String wholeID = DocumentsContract.getDocumentId(uri);
        // Split at colon, use second item in the array
        String id = wholeID.split(":")[1];
        String[] column = {MediaStore.Images.Media.DATA};
        // where id is equal to
        String sel = MediaStore.Images.Media._ID + "=?";
        Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                column, sel, new String[]{id}, null);

        int columnIndex = cursor.getColumnIndex(column[0]);

        if (cursor.moveToFirst()) {
            filePath = cursor.getString(columnIndex);
        }
        cursor.close();
        return filePath;
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }


    public void RequestPermission(Activity thisActivity, String Permission, int Code) {
        if (ContextCompat.checkSelfPermission(thisActivity,
                Permission)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(thisActivity,
                    Permission)) {
            } else {
                ActivityCompat.requestPermissions(thisActivity,
                        new String[]{Permission},
                        Code);
            }
        }
    }

    public boolean CheckPermission(Context context, String Permission) {
        if (ContextCompat.checkSelfPermission(context,
                Permission) == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkAndRequestPermissions() {
        int permissionCamera = ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA);

        int permissionRead = ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE);

        int permissionWrite = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        List<String> listPermissionsNeeded = new ArrayList<>();

        if (permissionCamera != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (permissionRead != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (permissionWrite != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this,
                    listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
                    123);
            return false;
        }
        return true;
    }


}