package ds.courier.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import org.osmdroid.bonuspack.location.GeocoderNominatim;
import org.osmdroid.events.DelayedMapListener;
import org.osmdroid.events.MapListener;
import org.osmdroid.events.ScrollEvent;
import org.osmdroid.events.ZoomEvent;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.compass.CompassOverlay;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

import java.io.IOException;

import ds.courier.R;

public class WorkMapActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, NavigationView.OnNavigationItemSelectedListener {
    private static final int MULTIPLE_PERMISSION_REQUEST_CODE = 4;
    static Context context;
    boolean isFirstLocation = true;
    boolean isAddressBEnable = true;
    EditText editAddress, editAddressB;
    RelativeLayout relative_b;
    TextView textSuccsess;
    ImageView btnStart;
    ProgressBar progressBar, progressBarB;
    Marker markerTAksi;
    Marker markerTAksi2;
    double latA = 0, lonA, latB, lonB;
    boolean isFirstStart = true;
    ImageView marker;
    ImageView marker_b;
    int map;
    Button btnSave;
    Button btnUpdate;
    boolean poisk = false;
    private MapView mapView;
    private Location mLastLocation;
    private GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_work);
        map = getIntent().getIntExtra("map", 0);
        context = this;
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        editAddress = (EditText) findViewById(R.id.edit_address);
        editAddressB = (EditText) findViewById(R.id.edit_address_b);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        progressBarB = (ProgressBar) findViewById(R.id.progress_bar_b);
        btnStart = (ImageView) findViewById(R.id.btnStart);
        marker = (ImageView) findViewById(R.id.marker);
        marker_b = (ImageView) findViewById(R.id.marker_b);
        relative_b = (RelativeLayout) findViewById(R.id.rv_b);
        btnSave = (Button) findViewById(R.id.btnSave);
        btnUpdate = (Button) findViewById(R.id.btnUpdate);
        checkPermissionsState();
        if (map == 1) {
            relative_b.setVisibility(View.GONE);
            latA = getIntent().getDoubleExtra("latA", 0);
            lonA = getIntent().getDoubleExtra("lonA", 0);
            editAddress.setText(getIntent().getStringExtra("nameA"));
            if (latA != 0) {
                addMarker(new GeoPoint(latA, lonA));
                poisk = true;
                marker.setVisibility(View.GONE);
                btnStart.setVisibility(View.GONE);
                btnSave.setVisibility(View.GONE);
                btnUpdate.setVisibility(View.VISIBLE);
                runOnUiThread(() -> mapView.getController().setCenter(new GeoPoint(latA, lonA)));
            } else {
                runOnUiThread(() -> mapView.getController().setCenter(new GeoPoint(42.874197, 74.600374)));
            }
        } else {
            latA = getIntent().getDoubleExtra("latA", 0);
            lonA = getIntent().getDoubleExtra("lonA", 0);
            latB = getIntent().getDoubleExtra("latB", 0);
            lonB = getIntent().getDoubleExtra("lonB", 0);
            editAddress.setText(getIntent().getStringExtra("nameA"));
            editAddressB.setText(getIntent().getStringExtra("nameB"));
            if (latA != 0) {
                addMarker(new GeoPoint(latA, lonA));
                addMarker2(new GeoPoint(latB, lonB));
                poisk = true;
                marker.setVisibility(View.GONE);
                btnStart.setVisibility(View.GONE);
                btnSave.setVisibility(View.GONE);
                btnUpdate.setVisibility(View.VISIBLE);
                runOnUiThread(() -> mapView.getController().setCenter(new GeoPoint(latA, lonA)));
            } else {
                runOnUiThread(() -> mapView.getController().setCenter(new GeoPoint(42.874197, 74.600374)));
            }
        }

        btnUpdate.setOnClickListener(view -> {
            poisk = false;
            mapView.getOverlays().remove(markerTAksi);
            mapView.getOverlays().remove(markerTAksi2);
            editAddress.setText("");
            editAddressB.setText("");
            marker.setVisibility(View.VISIBLE);
            btnStart.setVisibility(View.VISIBLE);
            btnSave.setVisibility(View.GONE);
            btnUpdate.setVisibility(View.GONE);
        });

        editAddress.setOnEditorActionListener((textView, i, keyEvent) -> {

            reverseGeocodingName(editAddress.getText().toString());

            return false;
        });

        editAddressB.setOnEditorActionListener((textView, i, keyEvent) -> {

            reverseGeocodingName(editAddressB.getText().toString());

            return false;
        });
        btnSave.setOnClickListener(view -> {

            if (isFirstStart) {
                Intent intent = new Intent();
                intent.putExtra("latA", latA);
                intent.putExtra("lonA", lonA);
                intent.putExtra("nameA", editAddress.getText().toString());
                Log.e("LAT", latA + "");
                setResult(RESULT_OK, intent);
                finish();
            } else {
                Intent intent = new Intent();
                intent.putExtra("latA", latA);
                intent.putExtra("lonA", lonA);
                intent.putExtra("latB", latB);
                intent.putExtra("lonB", lonB);
                intent.putExtra("nameA", editAddress.getText().toString());
                intent.putExtra("nameB", editAddressB.getText().toString());
                Log.e("LAT", latA + "");
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        btnStart.setOnClickListener(view -> {
            if (isFirstStart) {
                if (editAddress.getText().toString().contains("Подождите!") || editAddress.getText().toString().equals("")) {
                    Toast.makeText(WorkMapActivity.this, "Подождите!", Toast.LENGTH_SHORT).show();
                } else {
                    addMarker(new GeoPoint(latA, lonA));
                    if (map == 1) {
                        btnStart.setVisibility(View.GONE);
                        btnSave.setVisibility(View.VISIBLE);
                        marker.setVisibility(View.GONE);
                        poisk = true;
                    } else {
                        isFirstStart = false;
                        marker.setVisibility(View.GONE);
                        marker_b.setVisibility(View.VISIBLE);
                    }

                }
            } else if (editAddress.getText().toString().contains("Подождите!") || editAddress.getText().toString().equals("")) {
                Toast.makeText(WorkMapActivity.this, "Подождите!", Toast.LENGTH_SHORT).show();
            } else {
                if (editAddressB.getText().toString().contains("Подождите!")) {
                    Toast.makeText(WorkMapActivity.this, "Подождите!", Toast.LENGTH_SHORT).show();
                } else {
                    addMarker2(new GeoPoint(latB, lonB));
                    String editAddrb = String.valueOf(editAddressB.getText().toString().equals(""));
                    if (editAddressB.getText().toString().length() > 0)
                        isAddressBEnable = true;
                    editAddress.getText().toString();
                    btnStart.setVisibility(View.GONE);
                    btnSave.setVisibility(View.VISIBLE);
                    marker_b.setVisibility(View.GONE);
                    poisk = true;
                }
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK) {
            textSuccsess.setText("Вызвать Level");

            getSupportActionBar().setTitle("Вызвать Level");
        }
    }

    //TODO gg
    private void checkPermissionsState() {
        int internetPermissionCheck = ContextCompat.checkSelfPermission(this,
                Manifest.permission.INTERNET);

        int networkStatePermissionCheck = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_NETWORK_STATE);

        int writeExternalStoragePermissionCheck = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);

        int coarseLocationPermissionCheck = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION);

        int fineLocationPermissionCheck = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);

        int wifiStatePermissionCheck = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_WIFI_STATE);

        if (internetPermissionCheck == PackageManager.PERMISSION_GRANTED &&
                networkStatePermissionCheck == PackageManager.PERMISSION_GRANTED &&
                writeExternalStoragePermissionCheck == PackageManager.PERMISSION_GRANTED &&
                coarseLocationPermissionCheck == PackageManager.PERMISSION_GRANTED &&
                fineLocationPermissionCheck == PackageManager.PERMISSION_GRANTED &&
                wifiStatePermissionCheck == PackageManager.PERMISSION_GRANTED) {

            setupMap();

        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{
                            Manifest.permission.INTERNET,
                            Manifest.permission.ACCESS_NETWORK_STATE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_WIFI_STATE},
                    MULTIPLE_PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MULTIPLE_PERMISSION_REQUEST_CODE: {
                if (grantResults.length > 0) {
                    boolean somePermissionWasDenied = false;
                    for (int result : grantResults) {
                        if (result == PackageManager.PERMISSION_DENIED) {
                            somePermissionWasDenied = true;
                        }
                    }
                    if (somePermissionWasDenied) {
                        Toast.makeText(this, "Cant load maps without all the permissions granted", Toast.LENGTH_SHORT).show();
                    } else {
                        setupMap();
                    }
                } else {
                    Toast.makeText(this, "Cant load maps without all the permissions granted", Toast.LENGTH_SHORT).show();
                }
                return;
            }

        }
    }


    private void setupMap() {

        mapView = (MapView) findViewById(R.id.mapview);
        mapView.setClickable(true);
        mapView.setMultiTouchControls(true);

        mapView.getController().setZoom(15);
        mapView.setTileSource(TileSourceFactory.DEFAULT_TILE_SOURCE);

        MyLocationNewOverlay oMapLocationOverlay = new MyLocationNewOverlay(mapView);
        mapView.getOverlays().add(oMapLocationOverlay);
        oMapLocationOverlay.enableFollowLocation();

        oMapLocationOverlay.enableMyLocation();
        oMapLocationOverlay.enableFollowLocation();


        CompassOverlay compassOverlay = new CompassOverlay(this, mapView);
        compassOverlay.enableCompass();
        mapView.getOverlays().add(compassOverlay);

        mapView.setMapListener(new DelayedMapListener(new MapListener() {
            public boolean onZoom(final ZoomEvent e) {
                MapView mapView = (MapView) findViewById(R.id.mapview);

                return true;
            }

            public boolean onScroll(final ScrollEvent e) {
                MapView mapView = (MapView) findViewById(R.id.mapview);

                if (poisk == false) {
                    reverseGeocoding(mapView.getMapCenter().getLatitude(), mapView.getMapCenter().getLongitude());
                }

                return true;
            }
        }, 1000));
    }


    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }


    public void addMarker(GeoPoint geoPoint) {
        if (markerTAksi != null) {
            mapView.getOverlays().remove(markerTAksi);
        }
        markerTAksi = new Marker(mapView);
        markerTAksi.setPosition(geoPoint);
        markerTAksi.setIcon(getResources().getDrawable(R.mipmap.ic_location));
        markerTAksi.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_CENTER);

        mapView.getOverlays().add(markerTAksi);
        mapView.invalidate();
        latA = geoPoint.getLatitude();
        lonA = geoPoint.getLongitude();
        Log.e("LAT", latA + "");
    }


    public void addMarker2(GeoPoint geoPoint) {
        if (markerTAksi2 != null) {
            mapView.getOverlays().remove(markerTAksi2);
        }
        markerTAksi2 = new Marker(mapView);
        markerTAksi2.setPosition(geoPoint);
        markerTAksi2.setIcon(getResources().getDrawable(R.mipmap.ic_location));

        markerTAksi2.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_CENTER);

        mapView.getOverlays().add(markerTAksi2);
        mapView.invalidate();
        latB = geoPoint.getLatitude();
        lonB = geoPoint.getLongitude();
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        try {

            if (isFirstLocation) {
                if (latA == 0) {
                    mapView.getController().setCenter(new GeoPoint(mLastLocation.getLatitude(), mLastLocation.getLongitude()));
                    mapView.getController().animateTo(new GeoPoint(mLastLocation.getLatitude(), mLastLocation.getLongitude()));

                    isFirstLocation = false;
                }
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }


    public void reverseGeocodingName(final String name) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                GeocoderNominatim geocoder = new GeocoderNominatim("ulankarimovv@gmail.com");
                try {
                    if (geocoder.getFromLocationName(name, 1).size() > 0) {
                        final Address adress = geocoder.getFromLocationName(name, 1).get(0);
                        Log.e("Cityy", adress.toString());

                        Log.e("HHLLLsDDSS", adress.getLatitude() + " " + adress.getLongitude());
                        runOnUiThread(() -> mapView.getController().setCenter(new GeoPoint(adress.getLatitude(), adress.getLongitude())));
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }

    public void reverseGeocoding(final double lat, final double lon) {
        if (isFirstStart) {
            latA = lat;
            lonA = lon;
            progressBar.setVisibility(View.VISIBLE);
            editAddress.setText("Подождите!");

        } else {
            latB = lat;
            lonB = lon;
            progressBarB.setVisibility(View.VISIBLE);
            editAddressB.setText("Подождите!");

        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                GeocoderNominatim geocoder = new GeocoderNominatim("ulankarimovv@gmail.com");
                try {
                    if (geocoder.getFromLocation(lat, lon, 1).size() > 0) {
                        Address adress = geocoder.getFromLocation(lat, lon, 1).get(0);
                        Log.e("Cityy", adress.toString());
                        String saddress = adress.toString();
                        String[] s = saddress.split(",");
                        for (int i = 0; i < s.length; i++) {
                            Log.e("City" + i, s[i]);
                        }
                        int indexOfDisplayNAme = 20;
                        String addressFull;
                        String[] number;
                        for (int i = 15; i < 25; i++) {
                            if (s[i].contains("display_name=")) {
                                indexOfDisplayNAme = i;
                            }
                        }
                        number = s[indexOfDisplayNAme].split("=");
                        addressFull = number[1];
                        for (int i = indexOfDisplayNAme + 1; i < 27; i++) {
                            if (s[i].trim().contains("Бишкек")) {
                                break;
                            } else addressFull = addressFull + "," + s[i];
                        }
                        final String finalAddressFull = addressFull;
                        runOnUiThread(() -> {
                            if (isFirstStart) {
                                progressBar.setVisibility(View.GONE);
                                editAddress.setText(finalAddressFull);
                            } else {
                                progressBarB.setVisibility(View.GONE);
                                editAddressB.setText(finalAddressFull);
                            }
                        });
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

}
