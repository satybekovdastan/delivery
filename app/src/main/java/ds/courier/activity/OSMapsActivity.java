package ds.courier.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import org.osmdroid.events.DelayedMapListener;
import org.osmdroid.events.MapListener;
import org.osmdroid.events.ScrollEvent;
import org.osmdroid.events.ZoomEvent;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapController;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.OverlayItem;
import org.osmdroid.views.overlay.ScaleBarOverlay;
import org.osmdroid.views.overlay.compass.CompassOverlay;
import org.osmdroid.views.overlay.mylocation.IMyLocationProvider;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;
import org.osmdroid.views.overlay.Marker;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ds.courier.MapItemizedOverlay;
import ds.courier.R;
import ds.courier.adapter.RVAddressAdapter;
import ds.courier.adapter.RVCompanyOrderAdapter;
import ds.courier.adapter.RVMyOrdersAddressAdapter;
import ds.courier.adapter.RVProductAdapter;
import ds.courier.api.ApiService;
import ds.courier.api.RetroClient;
import ds.courier.model.Address;
import ds.courier.model.Key;
import ds.courier.model.Order;
import ds.courier.model.OrderDetail;
import ds.courier.model.Product;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OSMapsActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private static final int MULTIPLE_PERMISSION_REQUEST_CODE = 4;
    private MapView mapView;
    private Location mLastLocation;
    private GoogleApiClient mGoogleApiClient;

    String geolocation = null;
    String address = null;
    Marker markerTAksi;
    double lat = 0, lon, latB, lonB;
    Button btn_google_map, btn_take;
    int id;
    boolean taken = false;
    TextView text_address, tv_title;
    RecyclerView recyclerView;
    ArrayList<Product> products;
    RVProductAdapter rvProductAdapter;

    ArrayList<Address> addressArrayList;
    RVMyOrdersAddressAdapter rvAddressAdapter;
    boolean myOrder = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_osmaps);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(R.string.app_name);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }


        btn_google_map = findViewById(R.id.btn_google_map);
        btn_take = findViewById(R.id.btn_take);
        text_address = findViewById(R.id.text_address);
        tv_title = findViewById(R.id.tv_title);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setFocusable(false);
        recyclerView.requestFocus();
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        products = new ArrayList<>();
        addressArrayList = new ArrayList<>();
        rvAddressAdapter = new RVMyOrdersAddressAdapter(this, addressArrayList);
        recyclerView.setAdapter(rvAddressAdapter);
        lat = 42.871959;
        lon = 74.611437;

        id = getIntent().getIntExtra("id", 0);

        Log.e("ID", id+"");
        checkPermissionsState();

//        geolocation = getIntent().getStringExtra("geolocation");
//        address = getIntent().getStringExtra("address");
//        boolean take = getIntent().getBooleanExtra("taken", false);

//        if (order.getIsTaken()){
//            taken = false;
//            btn_take.setText("Отказать");
//            btn_take.setVisibility(View.GONE);
//        }else {
//            taken = true;
//            btn_take.setText("Принять");
//            btn_google_map.setVisibility(View.GONE);
//        }

        btn_google_map.setVisibility(View.GONE);
        btn_google_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?daddr=" + geolocation));
                startActivity(intent);
            }
        });

        getJsonOrdersDetail();
        myOrder = getIntent().getBooleanExtra("myOrder", false);
        if (myOrder){
            btn_take.setText("Отказать");
        }else {
        }

        btn_take.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!myOrder) {
                    postTake();
                }else {
                    showDialogReject();
                }
            }
        });

//        showFoodJoint("FOOD", lat, lon, "NAME", "DES");

    }


    public void getJsonOrdersDetail() {

        ApiService api = RetroClient.getApiService(this);
        retrofit2.Call<OrderDetail> call;
        call = api.getOrderDetail(id);
        call.enqueue(new retrofit2.Callback<OrderDetail>() {
            @Override
            public void onResponse(retrofit2.Call<OrderDetail> call, final retrofit2.Response<OrderDetail> response) {

                Log.e("CODE", response.code()+"");
                if (response.isSuccessful()) {
                    OrderDetail order = response.body();
                    text_address.setText(address);
                    tv_title.setText(order.getTitle());

                    if (!order.getAddresses().isEmpty()) {
                        int count = 0;
                        addressArrayList.addAll(order.getAddresses());
                        rvAddressAdapter.notifyDataSetChanged();

                        for (Address address : order.getAddresses()) {
                            List<String> elephantList = Arrays.asList(address.getGeolocation().split(","));
                            for (int i = 0; i < elephantList.size(); i++) {
                                lat = Double.parseDouble(elephantList.get(0));
                                lon = Double.parseDouble(elephantList.get(1));
                            }
                            List<String> productTitle = new ArrayList<>();
                            for (Product product : address.getProducts()) {
                                productTitle.add(product.getTitle());
                            }
                            addMarker(new GeoPoint(lat, lon), count += 1, productTitle);
                        }

                    }
                }
            }

            @Override
            public void onFailure(retrofit2.Call<OrderDetail> call, Throwable t) {
                Toast.makeText(OSMapsActivity.this, t.getMessage() + "", Toast.LENGTH_SHORT).show();
                Log.e("TAG", "FAIL " + t.getMessage());
            }
        });
    }


    public void postTake() {
        final ProgressDialog uploading;
        uploading = ProgressDialog.show(OSMapsActivity.this, "Loading", "Please wait...", false, false);

        final ApiService api = RetroClient.getApiService(this);
        Call<Order> call;
        call = api.postTake(id);
        call.enqueue(new Callback<Order>() {
            @Override
            public void onResponse(Call<Order> call, Response<Order> response) {
                uploading.dismiss();
                Log.e("RES_CODE", response.code() + "");
                if (response.isSuccessful()) {
                    btn_take.setVisibility(View.GONE);
                } else {
                    uploading.dismiss();
                    try {
                        Log.e("ERROR", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Order> call, Throwable t) {
                uploading.dismiss();
                Log.e("FAIL", t.getMessage());
            }
        });
    }


    public void showDialogReject() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.allert_dialog_editext, null);

        builder.setView(dialogView);

        Button button = dialogView.findViewById(R.id.btnCancel);
        final EditText editText = dialogView.findViewById(R.id.edit_comment);

        final AlertDialog dialog = builder.create();
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String comment = editText.getText().toString();
                if (!comment.isEmpty()) {
                    postRejectOrder(comment);
                    dialog.cancel();
                }
            }
        });
//        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }


    public void postRejectOrder(String comment) {
        final ProgressDialog uploading;
        uploading = ProgressDialog.show(OSMapsActivity.this, "Loading", "Please wait...", false, false);

        final ApiService api = RetroClient.getApiService(this);
        Call<Order> call;
        call = api.postReject(comment , id);
        call.enqueue(new Callback<Order>() {
            @Override
            public void onResponse(Call<Order> call, Response<Order> response) {
                uploading.dismiss();
                Log.e("RES_CODE", response.code() + "");
                if (response.isSuccessful()) {
                    btn_take.setVisibility(View.GONE);

                } else {
                    uploading.dismiss();
                    try {
                        Log.e("ERROR", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Order> call, Throwable t) {
                uploading.dismiss();
                Log.e("FAIL", t.getMessage());
            }
        });
    }


    private void checkPermissionsState() {
        int internetPermissionCheck = ContextCompat.checkSelfPermission(this,
                Manifest.permission.INTERNET);

        int networkStatePermissionCheck = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_NETWORK_STATE);

        int writeExternalStoragePermissionCheck = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);

        int coarseLocationPermissionCheck = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION);

        int fineLocationPermissionCheck = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);

        int wifiStatePermissionCheck = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_WIFI_STATE);

        if (internetPermissionCheck == PackageManager.PERMISSION_GRANTED &&
                networkStatePermissionCheck == PackageManager.PERMISSION_GRANTED &&
                writeExternalStoragePermissionCheck == PackageManager.PERMISSION_GRANTED &&
                coarseLocationPermissionCheck == PackageManager.PERMISSION_GRANTED &&
                fineLocationPermissionCheck == PackageManager.PERMISSION_GRANTED &&
                wifiStatePermissionCheck == PackageManager.PERMISSION_GRANTED) {

            setupMap();

        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{
                            Manifest.permission.INTERNET,
                            Manifest.permission.ACCESS_NETWORK_STATE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_WIFI_STATE},
                    MULTIPLE_PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MULTIPLE_PERMISSION_REQUEST_CODE: {
                if (grantResults.length > 0) {
                    boolean somePermissionWasDenied = false;
                    for (int result : grantResults) {
                        if (result == PackageManager.PERMISSION_DENIED) {
                            somePermissionWasDenied = true;
                        }
                    }
                    if (somePermissionWasDenied) {
                        Toast.makeText(this, "Cant load maps without all the permissions granted", Toast.LENGTH_SHORT).show();
                    } else {
                        setupMap();
                    }
                } else {
                    Toast.makeText(this, "Cant load maps without all the permissions granted", Toast.LENGTH_SHORT).show();
                }
                return;
            }

        }
    }


    public void addMarker(GeoPoint geoPoint, int i, List<String> productTitle) {
//        if (markerTAksi != null) {
//            mapView.getOverlays().remove(markerTAksi);
//        }
        markerTAksi = new Marker(mapView);

        markerTAksi = new MarkerWithLabel(mapView, "" + i);

        String prod = "";
        int pos = 0;
        for (String product : productTitle) {
            pos += 1;
            prod += pos + ". " + product + "\n";
        }
        markerTAksi.setTitle("" + prod);

        markerTAksi.setPosition(geoPoint);
        markerTAksi.setIcon(getResources().getDrawable(R.mipmap.ic_location));
        markerTAksi.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_CENTER);
        mapView.getOverlays().add(markerTAksi);
        mapView.invalidate();
        double lat = geoPoint.getLatitude();
        double lon = geoPoint.getLongitude();
        mapView.getController().setCenter(new GeoPoint(lat, lon));
        mapView.getController().animateTo(new GeoPoint(lat, lon));
    }

    public void showFoodJoint(String foodJointId, double foodJointLat, double foodJointLon, String foodJointName, String foodJointDescription) {
        Drawable restaurantLocationDrawable = this.getResources().getDrawable(
                R.drawable.btn_moreinfo);
        MapItemizedOverlay itemizedoverlayForRestaurant = new MapItemizedOverlay(
                restaurantLocationDrawable, this);

        GeoPoint myPoint1 = new GeoPoint(foodJointLat, foodJointLon);
        OverlayItem overlayitem2 = new OverlayItem(foodJointId, foodJointName, foodJointDescription, myPoint1);

        itemizedoverlayForRestaurant.addOverlay(overlayitem2);
        mapView.getOverlays().add(itemizedoverlayForRestaurant);

//        mapView.add(itemizedoverlayForRestaurant);
    }

    public void getMapsCenterMarker(String geolocation) {
        double lat = 0;
        double lon = 0;
        List<String> elephantList = Arrays.asList(geolocation.split(","));
        for (int i = 0; i < elephantList.size(); i++) {
            lat = Double.parseDouble(elephantList.get(0));
            lon = Double.parseDouble(elephantList.get(1));
        }
        Log.e("LOCA CENTER", lat + " " + lon + " - " + geolocation);
        mapView.getController().setCenter(new GeoPoint(lat, lon));
        mapView.getController().animateTo(new GeoPoint(lat, lon));
    }

    public class MarkerWithLabel extends Marker {
        Paint textPaint = null;
        String mLabel = null;

        public MarkerWithLabel(MapView mapView, String label) {
            super(mapView);
            mLabel = label;
            textPaint = new Paint();
            textPaint.setColor(Color.RED);
            textPaint.setTextSize(30f);
            textPaint.setAntiAlias(true);
            textPaint.setTextAlign(Paint.Align.LEFT);
        }

        public void draw(final Canvas c, final MapView osmv, boolean shadow) {
            draw(c, osmv);
        }

        public void draw(final Canvas c, final MapView osmv) {
            super.draw(c, osmv, false);
            Point p = this.mPositionPixels;  // already provisioned by Marker
            c.drawText(mLabel, p.x - 13, p.y + 70, textPaint);
        }
    }

    @SuppressLint("ResourceAsColor")
    private void setupMap() {

        mapView = (MapView) findViewById(R.id.mapview);
        mapView.setClickable(true);
        mapView.setMultiTouchControls(true);
        mapView.setBackgroundColor(R.color.colorPrimary);
//        setContentView(mapView); //displaying the MapView

        mapView.getController().setZoom(15); //set initial zoom-level, depends on your need
//        mapView.getController().setCenter(ONCATIVO);
//        mapView.setUseDataConnection(false); //keeps the mapView from loading online tiles using network connection.
        mapView.setTileSource(TileSourceFactory.DEFAULT_TILE_SOURCE);

        MyLocationNewOverlay oMapLocationOverlay = new MyLocationNewOverlay(mapView);
        mapView.getOverlays().add(oMapLocationOverlay);
        oMapLocationOverlay.enableFollowLocation();
        oMapLocationOverlay.enableMyLocation();
        oMapLocationOverlay.enableFollowLocation();

        CompassOverlay compassOverlay = new CompassOverlay(this, mapView);
        compassOverlay.enableCompass();
        mapView.getOverlays().add(compassOverlay);

        mapView.setMapListener(new DelayedMapListener(new MapListener() {
            public boolean onZoom(final ZoomEvent e) {
                MapView mapView = (MapView) findViewById(R.id.mapview);

                String latitudeStr = "" + mapView.getMapCenter().getLatitude();
                String longitudeStr = "" + mapView.getMapCenter().getLongitude();

                String latitudeFormattedStr = latitudeStr.substring(0, Math.min(latitudeStr.length(), 7));
                String longitudeFormattedStr = longitudeStr.substring(0, Math.min(longitudeStr.length(), 7));

                Log.i("zoom", "" + mapView.getMapCenter().getLatitude() + ", " + mapView.getMapCenter().getLongitude());
                TextView latLongTv = (TextView) findViewById(R.id.textView);
                latLongTv.setText("" + latitudeFormattedStr + ", " + longitudeFormattedStr);
                return true;
            }

            public boolean onScroll(final ScrollEvent e) {
                MapView mapView = (MapView) findViewById(R.id.mapview);

                String latitudeStr = "" + mapView.getMapCenter().getLatitude();
                String longitudeStr = "" + mapView.getMapCenter().getLongitude();

                String latitudeFormattedStr = latitudeStr.substring(0, Math.min(latitudeStr.length(), 7));
                String longitudeFormattedStr = longitudeStr.substring(0, Math.min(longitudeStr.length(), 7));

                Log.i("scroll", "" + mapView.getMapCenter().getLatitude() + ", " + mapView.getMapCenter().getLongitude());
                TextView latLongTv = (TextView) findViewById(R.id.textView);
                latLongTv.setText("" + latitudeFormattedStr + ", " + longitudeFormattedStr);
                return true;
            }
        }, 1000));
    }

    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    private void setCenterInMyCurrentLocation() {
        if (mLastLocation != null) {
            Log.e("MY LOCATION", mLastLocation+"");
            mapView.getController().setCenter(new GeoPoint(mLastLocation.getLatitude(), mLastLocation.getLongitude()));
        } else {
            Toast.makeText(this, "Getting current location", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
//        mapView.getController().setCenter(new GeoPoint(lat, lon));
//        mapView.getController().animateTo(new GeoPoint(lat, lon));

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_maps, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_locate) {
            setCenterInMyCurrentLocation();
        }
        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

}
