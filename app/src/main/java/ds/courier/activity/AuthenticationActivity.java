package ds.courier.activity;


import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import ds.courier.R;
import ds.courier.TokenPrefsHelper;
import ds.courier.api.ApiService;
import ds.courier.api.RetroClient;
import ds.courier.model.Key;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AuthenticationActivity extends AppCompatActivity {

    private String mVerificationId;
    private EditText editTextCode;
    private FirebaseAuth mAuth;

    RelativeLayout container_number;
    EditText editTextMobile;
    String phone;
    boolean login = true;
    String token;
    TextView textTimer, textGetCodeAgain;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authentication);

        mAuth = FirebaseAuth.getInstance();
        editTextCode = findViewById(R.id.editTextCode);
        container_number = findViewById(R.id.container_number);

        textTimer = findViewById(R.id.tv_login_timer);
        textGetCodeAgain = findViewById(R.id.tv_get_code_again);

//        Intent intent = getIntent();
//        String mobile = intent.getStringExtra("mobile");
//        sendVerificationCode(mobile);

        editTextMobile = findViewById(R.id.editTextMobile);
        Button buttonContinue = findViewById(R.id.buttonContinue);

        buttonContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                String mobile = editTextMobile.getText().toString().trim();
                boolean bool = true;
                String number = editTextMobile.getText().toString().replaceAll("\\D+", "").trim();
                Log.e("SIZE", number.length() + "");
                if (number.length() == 12) {
                    if (number.contains("99655") || number.contains("99650") || number.contains("99677") || number.contains("99670") || number.contains("996312")) {
                        bool = true;
                    } else {
                        bool = false;
                        Toast.makeText(AuthenticationActivity.this, "Введен некорректный номер телефона.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    bool = false;
                    Toast.makeText(AuthenticationActivity.this, "Поле телефон не введено", Toast.LENGTH_SHORT).show();
                }

                if (bool) {
                    phone = "+"+number;
                    postLogin(phone);
//                    sendVerificationCode(number);
//                    container_number.setVisibility(View.GONE);
                }

            }
        });

        findViewById(R.id.buttonSignIn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code = editTextCode.getText().toString().trim();
                if (code.isEmpty() || code.length() < 6) {
                    editTextCode.setError("Enter valid code");
                    editTextCode.requestFocus();
                    return;
                }
                verifyVerificationCode(code);
            }
        });

        textGetCodeAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textGetCodeAgain.setVisibility(View.GONE);
                textTimer.setVisibility(View.VISIBLE);
                sendVerificationCode(phone);
            }
        });

    }


    public void postLogin(final String number) {
        final ApiService api = RetroClient.getApiService(this);
        Call<Key> call;
        Log.e("NUM", number+"");

        call = api.postLogin(number, number);
        call.enqueue(new Callback<Key>() {
            @Override
            public void onResponse(Call<Key> call, Response<Key> response) {
                Log.e("RES_CODE", response.code()+"");
                if (response.isSuccessful()) {
                    token = response.body().getToken();
                    sendVerificationCode(number);
                    container_number.setVisibility(View.GONE);
                    login = true;
//                    new TokenPrefsHelper().saveToken(AuthenticationActivity.this, token);
//                    Intent intent = new Intent(AuthenticationActivity.this, MainActivity.class);
//                    intent.putExtra("phone", phone);
//                    startActivity(intent);
                } else {
                    try {
                        login = false;
                        sendVerificationCode(number);
                        container_number.setVisibility(View.GONE);

//                        Intent intent = new Intent(AuthenticationActivity.this, RegistrationActivity.class);
//                        intent.putExtra("phone", phone);
//                        startActivity(intent);
                        Log.e("ERROR", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Key> call, Throwable t) {
                Log.e("FAIL", t.getMessage());
            }
        });
    }


    private void sendVerificationCode(String mobile) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                mobile,
                60,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mCallbacks);

        timerStart();
        textGetCodeAgain.setVisibility(View.GONE);

    }


    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

            String code = phoneAuthCredential.getSmsCode();

            if (code != null) {
                editTextCode.setText(code);
                verifyVerificationCode(code);
            }
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            Toast.makeText(AuthenticationActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
            Log.e("FAIL VERY", e.getMessage());
        }

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);
            mVerificationId = s;
        }
    };


    private void verifyVerificationCode(String code) {
        try {
            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, code);
            signInWithPhoneAuthCredential(credential);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(AuthenticationActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(AuthenticationActivity.this, "Успешно бутту братишка!", Toast.LENGTH_SHORT).show();
                            Intent intent;
                            if (login){
                                new TokenPrefsHelper().saveToken(AuthenticationActivity.this, token);
                                intent = new Intent(AuthenticationActivity.this, MainActivity.class);
                            }else {
                                intent = new Intent(AuthenticationActivity.this, RegistrationActivity.class);
                            }
                            intent.putExtra("phone", phone);
                            startActivity(intent);
                            finish();
                        } else {

                            String message = "Somthing is wrong, we will fix it soon...";

                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                message = "Invalid code entered...";
                            }

                            Toast.makeText(AuthenticationActivity.this, message + "", Toast.LENGTH_SHORT).show();

                        }
                    }
                });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }



    class CountDowntimer1 extends CountDownTimer {
        int a=59;
        public CountDowntimer1(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }


        @Override
        public void onTick(long l) {
            textTimer.setText("Запросить код еще раз через: "+(a));
            a--;
        }

        @Override
        public void onFinish() {
            textGetCodeAgain.setVisibility(View.VISIBLE);
            textTimer.setVisibility(View.GONE);
        }
    }
    public  void timerStart(){
        CountDowntimer1 countDowntimer1=new CountDowntimer1(60*1000,1000);
        countDowntimer1.start();
    }
}