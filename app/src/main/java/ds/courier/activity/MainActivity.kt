package ds.courier.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import ds.courier.*
import ds.courier.fragment.*


class MainActivity : AppCompatActivity() {

    internal lateinit var navigation: BottomNavigationView
    private var mSelectedItem: Int = 0
    private var fragment: Fragment? = null
    internal var itemSelect = 6
    internal var person = ""
    internal var bottomBar = false
    private val tokenPrefsHelper = TokenPrefsHelper()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (tokenPrefsHelper.getToken(this) == null) {
            val intent = Intent(this@MainActivity, AuthenticationActivity::class.java)
            startActivity(intent)
            finish()
        }else{
            Log.e("TOKEN", tokenPrefsHelper.getToken(this))
        }

        setContentView(R.layout.activity_main)

        navigation = findViewById(R.id.navigation)
        BottomNavigationViewHelper.disableShiftMode(navigation)

        window.setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        )

        navigation.setOnNavigationItemSelectedListener { item ->
            selectFragment(item)
            //                itemSelect = 0;
            true
        }

        fragment = CompaniesOrdersFragment()
        selectFragmentTransaction(fragment, "Заказы")
        itemSelect = 0
    }


    fun hideKeyboard(view: View) {
        val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }


    private fun selectFragment(item: MenuItem) {
        when (item.itemId) {
            R.id.navigation_one -> {
                if (itemSelect != 0) {
                    fragment = CompaniesOrdersFragment()
                    selectFragmentTransaction(fragment, "Заказы")
                    itemSelect = 0
                }
                return
            }
            R.id.navigation_two -> {
                if (itemSelect != 1) {
                    fragment = MyDeliveryFragment()
                    selectFragmentTransaction(fragment, "Мои заказы")
                    itemSelect = 1
                }
                return
            }
            R.id.navigation_three -> {
                if (itemSelect != 2) {
                    fragment = UserFragment()
                    selectFragmentTransaction(fragment, "Профиль")
                    itemSelect = 2
//                    tokenPrefsHelper.getDeleteToken(this)
//                    val intent = Intent(this@MainActivity, AuthenticationActivity::class.java)
//                    startActivity(intent)
//                    finish()
                }
                return
            }
        }

        mSelectedItem = item.itemId
        for (i in 0 until navigation.menu.size()) {
            val menuItem = navigation.menu.getItem(i)
            menuItem.isChecked = menuItem.itemId == item.itemId
        }
    }

    fun removeFragmentTransaction() {
        person = ""
        invalidateOptionsMenu()
        supportFragmentManager.beginTransaction().remove(fragment!!).commit()
    }

    fun selectFragmentTransaction(fragment: Fragment?, title: String) {
        val ft = supportFragmentManager.beginTransaction()
        if (fragment != null) {
            ft.replace(R.id.content, fragment)
            supportFragmentManager.popBackStack()
            ft.commit()
        }
    }


    override fun onBackPressed() {
        if (itemSelect != 0) {
            removeFragmentTransaction()
            itemSelect = 0
        } else if (itemSelect == 0) {
            super.onBackPressed()
        }
    }

    companion object {
        private val SELECTED_ITEM = "arg_selected_item"
    }

}
